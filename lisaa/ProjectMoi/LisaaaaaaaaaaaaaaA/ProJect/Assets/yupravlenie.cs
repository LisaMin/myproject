﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OVR;

public class yupravlenie : MonoBehaviour
{
    public Vector2 thumbstick;
    public bool IsTouched;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        thumbstick = OVRInput.Get(OVRInput.Axis2D.SecondaryThumbstick);
        if (OVRInput.GetDown(OVRInput.Button.Two))
        {
            print("Down");

        }
        if (OVRInput.GetUp(OVRInput.Button.Two))
        {
            print("Up");

        }
        IsTouched = OVRInput.Get(OVRInput.Touch.SecondaryIndexTrigger);

    }
}
