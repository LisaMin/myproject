﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kaver : MonoBehaviour
{
    public List<Material> Materials = new List<Material>();
    private Material def;
    // Start is called before the first frame update
    void Start()
    {
        def = gameObject.GetComponent<MeshRenderer>().material;
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.name == "key (3)")
            gameObject.GetComponent<MeshRenderer>().material = Materials[0];
        else
        gameObject.GetComponent<MeshRenderer>().material = def;
    }
}
