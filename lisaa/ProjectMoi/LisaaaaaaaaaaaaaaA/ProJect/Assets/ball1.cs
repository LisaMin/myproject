﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ball1 : MonoBehaviour
{
    public List<Material> Materials = new List<Material>();
    

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<MeshRenderer>().material = Materials[1];
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Sphere (1)")
            gameObject.GetComponent<MeshRenderer>().material = Materials[0];
     
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.name == "Sphere (1)")
           
            gameObject.GetComponent<MeshRenderer>().material = Materials[1];
    }
}
