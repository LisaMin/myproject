﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class picture2 : MonoBehaviour
{
    public List<Material> Materials = new List<Material>();
    public GameObject plane0;
    public GameObject plane1;
    public Material mat;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<MeshRenderer>().material = Materials[1];
    }

    // Update is called once per frame
    void Update()
    {
        if ((plane0.GetComponent<MeshRenderer>().material.color == mat.color) || (plane1.GetComponent<MeshRenderer>().material.color == mat.color))
            gameObject.GetComponent<MeshRenderer>().material = Materials[0];
        else gameObject.GetComponent<MeshRenderer>().material = Materials[1];
    }
}
