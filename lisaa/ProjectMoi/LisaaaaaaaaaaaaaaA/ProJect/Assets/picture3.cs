﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class picture3 : MonoBehaviour
{
    
    public GameObject picture1;
    public GameObject picture2;
    public Material mat;
    

    // Start is called before the first frame update
    void Start()
    {

        gameObject.transform.position = new Vector3(62.62f, 1.632259f, 32.72f);

    }

    // Update is called once per frame
    void Update()
    {
        
        if ((picture1.GetComponent<MeshRenderer>().material.mainTexture == mat.mainTexture ) && (picture2.GetComponent<MeshRenderer>().material.mainTexture == mat.mainTexture))
            gameObject.transform.position = new Vector3(62.62f, 1.632259f, 41);
        else gameObject.transform.position = new Vector3(62.62f, 1.632259f, 32.72f);


    }
}
