﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dver5 : MonoBehaviour
{
    public GameObject noga1;
    public GameObject noga2;
    public GameObject ryka1;
    public GameObject ryka2;
    public GameObject telo;
    public GameObject golova;
    public GameObject nos;
    public GameObject glaz1;
    public GameObject glaz2;
    public GameObject rot1;
    public GameObject rot2;
    public GameObject rot3;
    public Material matw;
    public Material matbl;
    public Material mator;
    public Material matbr;
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        if ((noga1.GetComponent<MeshRenderer>().material.color == matw.color) && (noga2.GetComponent<MeshRenderer>().material.color == matw.color) && (ryka1.GetComponent<MeshRenderer>().material.color == matbr.color) && (ryka2.GetComponent<MeshRenderer>().material.color == matbr.color) && (telo.GetComponent<MeshRenderer>().material.color == matw.color) && (golova.GetComponent<MeshRenderer>().material.color == matw.color) && (nos.GetComponent<MeshRenderer>().material.color == mator.color) && (glaz1.GetComponent<MeshRenderer>().material.color == matbl.color) && (glaz2.GetComponent<MeshRenderer>().material.color == matbl.color) && (rot1.GetComponent<MeshRenderer>().material.color == matbl.color) && (rot2.GetComponent<MeshRenderer>().material.color == matbl.color) && (rot3.GetComponent<MeshRenderer>().material.color == matbl.color))
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
