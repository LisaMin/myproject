﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class whitepen : MonoBehaviour
{
    public GameObject noga1;
    public GameObject noga2;
    public GameObject ryka1;
    public GameObject ryka2;
    public GameObject telo;
    public GameObject golova;
    public GameObject nos;
    public GameObject glaz1;
    public GameObject glaz2;
    public GameObject rot1;
    public GameObject rot2;
    public GameObject rot3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Sphere (9)")
        {
            noga1.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (10)")
        {
            noga2.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Cube (2)")
        {
            ryka1.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Cube (3)")
        {
            ryka2.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (2)")
        {
            telo.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (3)")
        {
            golova.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Capsule")
        {
            nos.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (4)")
        {
            glaz1.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (5)")
        {
            glaz2.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (8)")
        {
            rot1.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (6)")
        {
            rot2.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
        if (collision.gameObject.name == "Sphere (7)")
        {
            rot3.GetComponent<MeshRenderer>().material = gameObject.GetComponent<MeshRenderer>().material;
        }
    }
}
